package main

import (
	"encoding/json"
	"fmt"
	"github.com/spf13/viper"
	"io"
	"log"
	"net/http"
	"os"
	"reflect"
	"strconv"
	"strings"
)

type response struct {
	Hostname            string `json:"hostname"`
	Temperature         string `json:"temperature"`
	WeatherDescriptions string `json:"weather_descriptions"`
	WindSpeed           string `json:"wind_speed"`
	Humidity            string `json:"humidity"`
	FeelsLike           string `json:"feelslike"`
}

type config struct {
	Port int
	Url  string
}

func (m config) handler(w http.ResponseWriter, req *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	res := &response{}
	res.Hostname, _ = os.Hostname()
	body := m.fetchTemperatureStatus()
	var result map[string]interface{}
	err := json.Unmarshal([]byte(body), &result)
	if err != nil {
		log.Fatal("Error occurred in decoding json.", err)
		return
	}
	var i = reflect.ValueOf(result["current"])
	mapString := make(map[string]string)
	for _, key := range i.MapKeys() {
		value := i.MapIndex(key)
		strKey := fmt.Sprintf("%v", key.Interface())
		strValue := fmt.Sprintf("%v", value.Interface())
		mapString[strKey] = strings.TrimPrefix(strValue, "[")
	}
	res.FeelsLike = mapString["feelslike"]
	res.Humidity = mapString["humidity"]
	res.WindSpeed = mapString["wind_speed"]
	res.Temperature = mapString["temperature"]
	res.WeatherDescriptions = strings.TrimSuffix(mapString["weather_descriptions"], "]")
	resp, err := json.Marshal(res)
	if err != nil {
		log.Fatalln(err)
		return
	}
	_, _ = w.Write(resp)
}

func (m config) fetchTemperatureStatus() string {
	resp, err := http.Get(m.Url)
	if err != nil {
		log.Fatalln(err)
		return "Error occurred in calling underlying service"
	}
	defer func(Body io.ReadCloser) {
		err := Body.Close()
		if err != nil {
			log.Fatalln(err)
		}
	}(resp.Body)
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		log.Fatalln(err)
		return "Can't fetch response body"
	}
	log.Println("response : " + string(body))
	return string(body)
}

func main() {
	viper.SetConfigName("config")
	viper.AddConfigPath("../")
	viper.SetConfigType("yml")
	if err := viper.ReadInConfig(); err != nil {
		fmt.Printf("Error reading config file, %s", err)
	}
	conf := config{}
	err := viper.Unmarshal(&conf)
	if err != nil {
		fmt.Printf("Unable to decode into struct, %v", err)
	}
	log.Println("HTTP server started on port : " + strconv.Itoa(conf.Port))
	http.HandleFunc("/", conf.handler)
	err = http.ListenAndServe(":"+strconv.Itoa(conf.Port), nil)
	if err != nil {
		log.Println("Error occurred in starting server.", err)
		os.Exit(1)
	}
	fmt.Println("hello world")
}
