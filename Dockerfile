FROM golang:1.18-alpine AS build

LABEL maintainer="Mohammadali Keshtparvar"

WORKDIR /app

COPY go.mod go.sum ./

RUN go mod download

COPY . .

WORKDIR /app/cmd

RUN CGO_ENABLED=0 GO_OS=linux go build -a -installsuffix cgo -o /out .

#download alpine image to run excution go file
FROM alpine:3.15

RUN apk --no-cache add ca-certificates

WORKDIR /app/cmd

COPY --from=build /out .

COPY config.yml /app

CMD ["./out"]